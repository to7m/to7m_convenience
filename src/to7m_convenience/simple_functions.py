from collections import namedtuple


class _Xargs(tuple):
    def __repr__(self):
        return f"_Xargs({', '.join(map(repr, self))})"


class _Kwargs(dict):
    def __repr__(self):
        return f"_Kwargs({super().__repr__()})"


_XargsAndKwargs = namedtuple("_XargsAndKwargs", ("xargs", "kwargs"))


def pass_fn():
    "takes no arguments, does nothing"

    pass


def ignore(*xargs, **kwargs):
    "takes arguments (making it slower than pass_fn), does nothing"

    pass


def return_fn(*xargs, **kwargs):
    if xargs:
        if kwargs:
            return _XargsAndKwargs(_Xargs(xargs), _Kwargs(kwargs))
        else:
            if len(xargs) == 1:
                return xargs[0]
            else:
                return _Xargs(xargs)
    else:
        if kwargs:
            return _Kwargs(kwargs)
        else:
            return _Xargs()


def fnn(*xargs):
    """
    fnn stands for “first not-None”. The returned value is the first passed
    argument whose identity is not that of None.
    """

    for arg in xargs:
        if arg is not None:
            return arg

    raise TypeError(f"all passed arguments {xargs} are None")
